# census

A tool to compare package contents with the file system on your computer.

Find changed files on your file system by checking what *should* be there
according to your package manager.

## compile

Build dependencies:
* `libarchive`
* `jsoncpp`
* `apt-pkg` (optional, required for "system" command on apt-based distros)
* `gtest` (optional, disable by appending `-Dwith_tests=disabled` at setup)

On Debian, these are installed by running
```{.sh}
$ sudo apt-get install libarchive-dev libjsoncpp-dev libapt-pkg-dev libgtest-dev
```

This is a meson based project. To compile it, run
```{.sh}
$ meson setup builddir
$ meson compile -C builddir
```

## example

To demonstrate, make a random, benign change to a file covered by a package
and run census to list all changes:
```{.sh}
$ echo >> /etc/default/cron
$ chmod 666 /etc/default/cron

# check single package:
$ census package /var/cache/apt/archives/cron_3.0pl1-162_amd64.deb

# or, check all installed packages:
$ census system
```

Output:
```{.json}
{
    "/etc/default/cron" : 
    [
        {
            "field" : "mode"
            "expected" : "33188",
            "actual" : "33206",
            "context" : "cron_3.0pl1-162_amd64.deb",
        },
        {
            "field" : "size"
            "expected" : "955",
            "actual" : "956",
            "context" : "cron_3.0pl1-162_amd64.deb",
        }
    ],
    (...)
}
```

Run `census --help` to see full list of commands.

## opens

* Output contains a lot of noise:
    * [usrmerge](https://wiki.debian.org/UsrMerge)
    * mesa/gl redirection,
    * packages setting file ownership during installation for some
      system user

* Reverse-search (finding files unaccounted for by scanning packages) is
  completely useless
    * files created / modified by {pre,post}inst scripts,
    * `.pyc` files everywhere,
    * systemd services dependency files,
    * `/etc/alternatives`,
    * `/var/{cache,log}`,
    * `/home`, `/boot`, `/dev`, `/opt`, `/tmp`, etc.
    * can filter out many, but not nearly enough.
    * Support user customizable filters?

* hardlinks in tar balls are not working properly. See Debian package
  `bzip2_1.0.8-5+b1_amd64.deb` for example.

* Reading list of installed packages currently only works on dpkg-based systems
  such as Debian and Ubuntu. Need to add support for rpm-based systems.

* Check ext2-specific attributes (`lsattr`)

* Check extended attributes (`getfattr`)

* Currently ignoring attributes {a,b,c,m}-time, hardlinks, rdev

* Bottleneck seems to be CPU. It should be quite simple to use multiple threads
  to increase performance.

* Repositories typically only have the latest (few) version(s) of a package.
  If the installed version of a package is too old, this tool is unable to
  download it. Check if https://snapshot.debian.org/ has an api to search
  for package version directly instead of "date when this package was recent".

* Add a flag to the CLI that disallows downloads, i.e. "offline mode".

* Output in different formats, e.g. CSV and XML.

* Differences in "mode" should be printed in octal.

## license

Copyright (C) 2023 Tim Wiederhake.

This code is licensed under the terms and conditions of the [GNU General Public
License v2.0 or later](https://spdx.org/licenses/GPL-2.0-or-later.html).

Report bugs and issues [here](https://gitlab.com/twied/census/-/issues).
