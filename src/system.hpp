// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include "diff.hpp"

#include <string>
#include <vector>

namespace census {

auto system_package_manager() -> std::string;

auto system_list() -> std::vector<std::string>;

auto system_validate() -> DiffMap;

} /* namespace census */
