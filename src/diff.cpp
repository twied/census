// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "diff.hpp"

#include <sstream>

namespace census {

template<typename T>
auto format(const T& value) -> std::string {
    auto stream = std::stringstream {};
    stream << value;
    return stream.str();
}

template<typename T>
auto do_diff(
        DiffList& result,
        const T& expected,
        const T& actual,
        std::string name,
        std::string context) {

    if (expected == actual) {
        return;
    }

    result.emplace_back(
            std::move(name),
            format(expected),
            format(actual),
            std::move(context));
}

auto diff(
        DiffList& result,
        const FileInfo& expected,
        const FileInfo& actual,
        const std::string& context) -> void {

    do_diff(result, expected.uid(), actual.uid(), "uid", context);
    do_diff(result, expected.gid(), actual.gid(), "gid", context);
    do_diff(result, expected.mode(), actual.mode(), "mode", context);
    do_diff(result, expected.size(), actual.size(), "size", context);
}

auto diff(
        DiffList& result,
        const Blob& expected,
        const Blob& actual,
        const std::string& context) -> void {

    for (size_t i = 0; i < std::min(expected.size(), actual.size()); ++i) {
        if (expected.data()[i] == actual.data()[i]) {
            continue;
        }

        result.emplace_back(
                "data at " + std::to_string(i),
                std::to_string(static_cast<int>(expected.data()[i])),
                std::to_string(static_cast<int>(actual.data()[i])),
                context);
        break;
    }
}

auto diff(
        DiffList& result,
        const File& expected,
        const File& actual,
        const std::string& context) -> void {

    do_diff(result, expected.path(), actual.path(), "path", context);
    diff(result, expected.fileinfo(), actual.fileinfo(), context);
    diff(result, expected.content(), actual.content(), context);
}

auto validate(
        DiffMap& result,
        const std::vector<File>& files,
        const std::string& context) -> void {

    for (const auto& expected : files) {
        const auto& path = expected.path();

        auto actual = File {};
        try {
            actual = File { path };
        } catch (const std::exception& e) {
            result[path].emplace_back("open", "ok", e.what(), context);
            continue;
        }

        diff(result[path], expected, actual, context);
    }
}

} /* namespace census */
