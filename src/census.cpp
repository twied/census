// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "census.hpp"

#include "diff.hpp"
#include "package.hpp"
#include "system.hpp"
#include "util.hpp"

#include <getopt.h>
#include <iostream>
#include <json/json.h>

static constexpr auto usage_string = std::string_view {
    "Usage: census [OPTIONS]... COMMAND\n"
    "Check system integrity by comparing the file system with\n"
    "installed packages' content.\n"
    "\n"
    "Options:\n"
    "  -h, --help\n"
    "        Alias for command 'help'.\n"
    "  -V, --version\n"
    "        Alias for command 'version'.\n"
    "  -v, --verbose\n"
    "        Show verbose output.\n"
    "\n"
    "Available commands:\n"
    "  help\n"
    "        Display this help and exit.\n"
    "  version\n"
    "        Display version information and exit.\n"
    "  list\n"
    "        Enumerate currently installed packages.\n"
    "  package FILE...\n"
    "        Validate individual packages.\n"
    "  system\n"
    "        Validate full system.\n"
};

using namespace std::literals;

auto options() -> global_options& {
    static auto data = global_options {
        false,
    };

    return data;
}

static auto print_diffmap(const census::DiffMap& results) -> int {
    auto retval = 0;

    auto json_root = Json::Value { Json::ValueType::objectValue };
    for (const auto& [key, val] : results) {
        if (val.empty() && !options().verbose) {
            continue;
        }

        retval = val.empty() ? retval : 1;

        auto json_file = Json::Value { Json::ValueType::arrayValue };

        for (const auto& difference : val) {
            auto json_element = Json::Value { Json::ValueType::objectValue };
            json_element["field"] = difference.field();
            json_element["expected"] = difference.expected();
            json_element["actual"] = difference.actual();
            if (!difference.context().empty()) {
                json_element["context"] = difference.context();
            }

            json_file.append(std::move(json_element));
        }

        json_root[key] = std::move(json_file);
    }

    std::cout << json_root << '\n';
    return retval;
}

static auto print_list(const std::vector<std::string>& results) -> void {
    auto json_root = Json::Value { Json::ValueType::arrayValue };
    for (const auto& package : results) {
        json_root.append(package);
    }

    std::cout << json_root << '\n';
}

static auto cmd_help() -> void {
    std::cout << usage_string;
}

static auto cmd_version() -> void {
    std::cout
            << "Census 0.1\n"
            << "Copyright (C) 2023 Tim Wiederhake\n"
            << "Backend: "
            << census::system_package_manager() << '\n'
            << "Report bugs to https://gitlab.com/twied/census/-/issues\n";
}

static auto cmd_package(char* argv[]) -> int {
    auto results = census::DiffMap {};
    for (auto* arg = argv; *arg != nullptr; ++arg) {
        census::validate_package(results, *arg);
    }

    return print_diffmap(results);
}

static auto cmd_list() -> int {
    auto package_list = census::system_list();
    print_list(package_list);
    return 0;
}

static auto cmd_system() -> int {
    auto results = census::system_validate();
    return print_diffmap(results);
}

static auto cmd(char* argv[]) -> int {
    if (argv[0] == "help"sv) {
        cmd_help();
        return 0;
    }

    if (argv[0] == "version"sv) {
        cmd_version();
        return 0;
    }

    if (argv[0] == "list"sv) {
        return cmd_list();
    }

    if (argv[0] == "package"sv) {
        return cmd_package(argv + 1);
    }

    if (argv[0] == "system"sv) {
        return cmd_system();
    }

    std::cerr << "Error: Unknown command\n";
    return 1;
}

static auto main_wrapped(int argc, char* argv[]) -> int {
    const auto* opt_short = ":hVv";
    option opt_long[] {
        { "help", no_argument, nullptr, 'h' },
        { "version", no_argument, nullptr, 'V' },
        { "verbose", no_argument, nullptr, 'v' },
        { nullptr, 0, nullptr, 0 }
    };

    auto short_help = false;
    auto short_version = false;

    while (true) {
        auto idx = -1;
        auto arg = getopt_long(
                argc,
                argv,
                opt_short,
                static_cast<option*>(opt_long),
                &idx);

        if (arg < 0) {
            break;
        }

        switch (arg) {
        case 'h':
            short_help = true;
            break;

        case 'V':
            short_version = true;
            break;

        case 'v':
            options().verbose = true;
            break;

        case ':':
            std::cerr << "Error: Missing argument for option\n";
            return 1;

        default:
            std::cerr << "Error: Invalid argument\n";
            return 1;
        }
    }

    if (short_help) {
        cmd_help();
        return 0;
    }

    if (short_version) {
        cmd_version();
        return 0;
    }

    if (optind >= argc) {
        cmd_help();
        return 1;
    }

    return cmd(argv + optind);
}

auto main(int argc, char* argv[]) -> int {
    try {
        return main_wrapped(argc, argv);
    } catch (const std::exception& e) {
        std::cerr << "Error: " << e.what() << '\n';
    } catch (...) {
        std::cerr << "Error: Exception thrown\n";
    }

    return 1;
}
