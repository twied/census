// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "package.hpp"

#include <archive.h>
#include <archive_entry.h>

#include <cstring>
#include <memory>
#include <stdexcept>

namespace census {

class Deleter {
public:
    void operator()(archive* ptr) const {
        if (ptr == nullptr) {
            return;
        }

        archive_read_free(ptr);
    }
};

static auto read_archive_entry(
        struct archive* archive,
        struct archive_entry* entry)
        -> File {

    auto file = File {};

    const auto* pathname = archive_entry_pathname(entry);
    if (pathname == nullptr) {
        throw std::runtime_error { "archive entry without name" };
    }

    if (pathname[0] == '.' && pathname[1] == '/') {
        pathname += 1;
    }

    file.path() = pathname;

    while (file.path().length() > 1 && file.path().back() == '/') {
        file.path().pop_back();
    }

    if (archive_entry_size_is_set(entry) == 0) {
        throw std::runtime_error { "archive entry without size" };
    }

    auto size = archive_entry_size(entry);
    file.fileinfo().size() = size;

    file.fileinfo().uid() = archive_entry_uid(entry);
    file.fileinfo().gid() = archive_entry_gid(entry);
    file.fileinfo().mode() = archive_entry_mode(entry);

    if (size > 0) {
        file.content() = Blob { static_cast<size_t>(size) };

        while (true) {
            auto size = size_t {};
            auto offset = la_int64_t {};
            const auto* buffer = static_cast<const void*>(nullptr);

            auto ret = archive_read_data_block(
                    archive,
                    &buffer,
                    &size,
                    &offset);

            if (ret != ARCHIVE_OK) {
                if (ret == ARCHIVE_EOF) {
                    break;
                }

                throw std::runtime_error {
                    archive_error_string(archive)
                };
            }

            std::memcpy(file.content().data() + offset, buffer, size);
        }
    }

    if ((file.fileinfo().mode() & AE_IFLNK) == AE_IFLNK) {
        auto data = std::string { archive_entry_symlink(entry) };
        file.fileinfo().size() = data.size();
    }

    return file;
};

auto read_archive(const Blob& blob) -> std::vector<File> {
    auto vector = std::vector<File> {};

    auto archive = std::unique_ptr<struct archive, Deleter> {
        archive_read_new()
    };

    auto ret = 0;

    ret = archive_read_support_format_all(archive.get());
    if (ret != ARCHIVE_OK) {
        throw std::runtime_error { archive_error_string(archive.get()) };
    }

    ret = archive_read_support_filter_all(archive.get());
    if (ret != ARCHIVE_OK) {
        throw std::runtime_error { archive_error_string(archive.get()) };
    }

    ret = archive_read_open_memory(archive.get(), blob.data(), blob.size());
    if (ret != ARCHIVE_OK) {
        throw std::runtime_error { archive_error_string(archive.get()) };
    }

    while (true) {
        auto* entry = static_cast<archive_entry*>(nullptr);

        ret = archive_read_next_header(archive.get(), &entry);
        if (ret != ARCHIVE_OK) {
            if (ret == ARCHIVE_EOF) {
                break;
            }

            throw std::runtime_error { archive_error_string(archive.get()) };
        }

        vector.push_back(read_archive_entry(archive.get(), entry));
    }

    return vector;
}

auto read_archive_deb(const Blob& blob) -> std::vector<File> {
    auto deb_version_blob = Blob { "2.0\n" };

    auto package = read_archive(blob);
    if ((package.size() != 3)
        || (package[0].path() != "debian-binary")
        || (package[0].content() != deb_version_blob)) {
        throw std::runtime_error { "Invalid package format" };
    }

    return read_archive(package[2].content());
}

[[nodiscard]] static auto endswith(
        const std::string& haystack,
        const std::string& needle) noexcept -> bool {
    auto needle_size = needle.size();
    auto haystack_size = haystack.size();

    if (needle_size > haystack_size) {
        return false;
    }

    return haystack.substr(haystack_size - needle_size) == needle;
}

auto validate_package(DiffMap& result, const File& package) -> void {
    auto files = std::vector<File> {};

    try {
        if (endswith(package.path(), ".deb")) {
            files = read_archive_deb(package.content());
        } else {
            files = read_archive(package.content());
        }
    } catch (const std::exception& e) {
        result[package.path()].emplace_back("package", "ok", e.what());
        return;
    }

    validate(result, files, package.path());
}

auto validate_package(
        DiffMap& result,
        const std::string& package) -> void {
    auto file = File {};
    try {
        file = File { package };
    } catch (const std::exception& e) {
        result[package].emplace_back("package", "ok", e.what());
        return;
    }

    validate_package(result, file);
}

} /* namespace census */
