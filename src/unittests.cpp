// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "unittests.hpp"

auto Environment::SetUp() -> void {
    m_pwd = std::filesystem::temp_directory_path() / "census";
    std::filesystem::create_directories(m_pwd);
    std::filesystem::current_path(m_pwd);

    auto empty_file = std::ofstream { "empty_file" };
    std::filesystem::create_directory("empty_dir");

    std::ofstream { "link_tgt" } << "link content";
    std::filesystem::create_symlink("link_tgt", "link_src");
}

auto Environment::TearDown() -> void {
    if (m_pwd.empty()) {
        return;
    }

    std::filesystem::remove_all(m_pwd);
}

TEST(Fd, Open) {
    auto dummy = census::Fd {};

    EXPECT_ANY_THROW((dummy = census::Fd { "nonexistent", O_RDONLY, 0 }));

    EXPECT_NO_THROW((dummy = census::Fd { "empty_file", O_RDONLY, 0 }));
    EXPECT_NO_THROW((dummy = census::Fd { "empty_dir", O_RDONLY, 0 }));

    EXPECT_NO_THROW((dummy = census::Fd { "link_tgt", O_RDONLY, 0 }));
    EXPECT_NO_THROW((dummy = census::Fd { "link_src", O_RDONLY, 0 }));
}

TEST(Blob, Empty) {
    auto blob = census::Blob {};
    EXPECT_EQ(blob.size(), 0);
    EXPECT_EQ(blob.data(), nullptr);

    blob = census::Blob { 1 };
    EXPECT_EQ(blob.size(), 1);
    EXPECT_NE(blob.data(), nullptr);
}

TEST(Blob, Memory) {
    auto string = std::string { "test" };
    auto blob = census::Blob { string.c_str(), string.size() };
    EXPECT_EQ(blob.size(), 4);
    EXPECT_NE(blob.data(), nullptr);
    EXPECT_EQ(blob.data()[0], std::byte { 't' });
    EXPECT_EQ(blob.data()[1], std::byte { 'e' });
    EXPECT_EQ(blob.data()[2], std::byte { 's' });
    EXPECT_EQ(blob.data()[3], std::byte { 't' });

    EXPECT_NO_THROW(static_cast<void>(census::Blob { nullptr, 0 }));
    EXPECT_ANY_THROW(static_cast<void>(census::Blob { nullptr, 1 }));
}

TEST(Blob, String) {
    auto string = std::string { "test" };
    auto blob1 = census::Blob { string.c_str(), string.size() };
    auto blob2 = census::Blob { string };
    EXPECT_EQ(blob1, blob2);
}

TEST(Blob, Fd) {
    auto fd_src = census::Fd { "link_src", O_RDONLY, 0 };
    auto fd_tgt = census::Fd { "link_tgt", O_RDONLY, 0 };
    auto blob_1 = census::Blob { fd_src, 4 };
    auto blob_2 = census::Blob { fd_tgt, 4 };
    auto blob_3 = census::Blob { "link" };
    EXPECT_EQ(blob_1, blob_2);
    EXPECT_EQ(blob_2, blob_3);

    auto fd_nofollow = census::Fd {
        "link_src",
        O_RDONLY | O_PATH | O_NOFOLLOW,
        0
    };
    EXPECT_ANY_THROW(static_cast<void>(census::Blob { fd_nofollow, 4 }));

    auto fd_dir = census::Fd { "empty_dir", O_RDONLY, 0 };
    EXPECT_ANY_THROW(static_cast<void>(census::Blob { fd_dir, 4 }));

    auto fd_dir_nofollow = census::Fd {
        "empty_dir",
        O_RDONLY | O_PATH | O_NOFOLLOW,
        0
    };
    EXPECT_ANY_THROW(static_cast<void>(census::Blob { fd_dir_nofollow, 4 }));
}

TEST(File, Open) {
    auto file_dir = census::File { "empty_dir" };
    EXPECT_EQ(file_dir.content(), census::Blob {});

    auto file_file = census::File { "link_tgt" };
    EXPECT_EQ(file_file.content(), census::Blob { "link content" });

    auto file_link = census::File { "link_src" };
    EXPECT_EQ(file_link.content(), census::Blob { "link_tgt" });

    EXPECT_ANY_THROW(static_cast<void>(census::File { "nonexistent" }));
}

TEST(Diff, File) {
    auto file_dir = census::File { "empty_dir" };
    auto file_file = census::File { "link_tgt" };
    auto file_link = census::File { "link_src" };

    EXPECT_EQ(census::diff(file_dir, file_dir).size(), 0);
    EXPECT_GT(census::diff(file_dir, file_file).size(), 1);
    EXPECT_GT(census::diff(file_dir, file_link).size(), 1);

    EXPECT_GT(census::diff(file_file, file_dir).size(), 1);
    EXPECT_EQ(census::diff(file_file, file_file).size(), 0);
    EXPECT_GT(census::diff(file_file, file_link).size(), 1);

    EXPECT_GT(census::diff(file_link, file_dir).size(), 1);
    EXPECT_GT(census::diff(file_link, file_file).size(), 1);
    EXPECT_EQ(census::diff(file_link, file_link).size(), 0);
}

auto main(int argc, char* argv[]) -> int {
    try {
        testing::InitGoogleTest(&argc, argv);

        /* NOLINTNEXTLINE(*-owning-memory): gtest takes ownership */
        testing::AddGlobalTestEnvironment(new Environment());

        return RUN_ALL_TESTS();
    } catch (const std::exception& e) {
        std::cerr << e.what() << '\n';
    }

    return 1;
}
