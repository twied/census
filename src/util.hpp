// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include <cstddef>
#include <cstdint>
#include <string>
#include <string_view>
#include <utility>

namespace census {

class FileInfo {
public:
    [[nodiscard]] FileInfo() noexcept = default;

    FileInfo(const FileInfo&) = delete;

    [[nodiscard]] FileInfo(FileInfo&&) noexcept = default;

    auto operator=(const FileInfo&) -> FileInfo& = delete;

    auto operator=(FileInfo&&) noexcept -> FileInfo& = default;

    ~FileInfo() = default;

    friend auto swap(FileInfo& lhs, FileInfo& rhs) noexcept -> void {
        std::swap(lhs.m_uid, rhs.m_uid);
        std::swap(lhs.m_gid, rhs.m_gid);
        std::swap(lhs.m_mode, rhs.m_mode);
        std::swap(lhs.m_size, rhs.m_size);
    }

    [[nodiscard]] auto operator==(const FileInfo& rhs) const noexcept -> bool {
        return (m_uid == rhs.m_uid)
                && (m_gid == rhs.m_gid)
                && (m_mode == rhs.m_mode)
                && (m_size == rhs.m_size);
    }

    [[nodiscard]] auto operator!=(const FileInfo& rhs) const noexcept -> bool {
        return !(*this == rhs);
    }

    [[nodiscard]] auto uid() const noexcept -> const uint32_t& {
        return m_uid;
    }

    [[nodiscard]] auto uid() noexcept -> uint32_t& {
        return m_uid;
    }

    [[nodiscard]] auto gid() const noexcept -> const uint32_t& {
        return m_gid;
    }

    [[nodiscard]] auto gid() noexcept -> uint32_t& {
        return m_gid;
    }

    [[nodiscard]] auto mode() const noexcept -> const uint32_t& {
        return m_mode;
    }

    [[nodiscard]] auto mode() noexcept -> uint32_t& {
        return m_mode;
    }

    [[nodiscard]] auto size() const noexcept -> const uint64_t& {
        return m_size;
    }

    [[nodiscard]] auto size() noexcept -> uint64_t& {
        return m_size;
    }

private:
    uint32_t m_uid { ~0U };
    uint32_t m_gid { ~0U };
    uint32_t m_mode { ~0U };
    uint64_t m_size { ~0UL };
};

class Fd {
public:
    [[nodiscard]] Fd() noexcept = default;

    [[nodiscard]] explicit Fd(int&& filedescriptor) noexcept :
        m_fd { filedescriptor } {
    }

    [[nodiscard]] explicit Fd(const std::string& path, int flags) :
        Fd { path, flags, 0 } {
    }

    [[nodiscard]] explicit Fd(
            const std::string& path,
            int flags,
            int mode);

    Fd(const Fd&) = delete;

    [[nodiscard]] Fd(Fd&& other) noexcept {
        swap(*this, other);
    }

    auto operator=(const Fd& other) -> Fd& = delete;

    auto operator=(Fd&& other) noexcept -> Fd& {
        swap(*this, other);
        return *this;
    }

    ~Fd();

    friend auto swap(Fd& lhs, Fd& rhs) noexcept -> void {
        std::swap(lhs.m_fd, rhs.m_fd);
    }

    [[nodiscard]] auto operator==(const Fd& rhs) const noexcept -> bool {
        return m_fd == rhs.m_fd;
    }

    [[nodiscard]] auto operator!=(const Fd& rhs) const noexcept -> bool {
        return !(*this == rhs);
    }

    [[nodiscard]] auto get() const noexcept -> int {
        return m_fd;
    }

    [[nodiscard]] auto release() noexcept -> int {
        auto ret = int { -1 };
        std::swap(m_fd, ret);
        return ret;
    }

private:
    int m_fd { -1 };
};

class Blob {
public:
    [[nodiscard]] Blob() noexcept = default;

    [[nodiscard]] explicit Blob(size_t size);

    [[nodiscard]] Blob(const void* ptr, size_t size);

    [[nodiscard]] explicit Blob(std::string_view string);

    [[nodiscard]] Blob(const Fd& filedescriptor, size_t size);

    Blob(const Blob&) = delete;

    [[nodiscard]] Blob(Blob&& other) noexcept {
        swap(*this, other);
    }

    auto operator=(const Blob&) -> Blob& = delete;

    auto operator=(Blob&& other) noexcept -> Blob& {
        swap(*this, other);
        return *this;
    }

    ~Blob();

    friend auto swap(Blob& lhs, Blob& rhs) noexcept -> void {
        std::swap(lhs.m_ptr, rhs.m_ptr);
        std::swap(lhs.m_size, rhs.m_size);
    }

    [[nodiscard]] auto operator==(const Blob& other) const noexcept -> bool;

    [[nodiscard]] auto operator!=(const Blob& other) const noexcept -> bool {
        return !(*this == other);
    }

    [[nodiscard]] auto data() const noexcept -> const std::byte* {
        return m_ptr;
    }

    [[nodiscard]] auto data() noexcept -> std::byte* {
        return m_ptr;
    }

    [[nodiscard]] auto size() const noexcept -> std::size_t {
        return m_size;
    }

    [[nodiscard]] auto operator!() const noexcept -> bool {
        return m_ptr == nullptr;
    }

private:
    std::byte* m_ptr {};
    std::size_t m_size {};
};

class File {
public:
    [[nodiscard]] File() noexcept = default;

    File(const File&) = delete;

    [[nodiscard]] explicit File(std::string path);

    [[nodiscard]] File(File&& other) noexcept {
        swap(*this, other);
    }

    auto operator=(const File&) -> File& = delete;

    auto operator=(File&& other) noexcept -> File& {
        swap(*this, other);
        return *this;
    }

    ~File() = default;

    friend auto swap(File& lhs, File& rhs) noexcept -> void {
        std::swap(lhs.m_path, rhs.m_path);
        std::swap(lhs.m_fileinfo, rhs.m_fileinfo);
        std::swap(lhs.m_content, rhs.m_content);
    }

    [[nodiscard]] auto operator==(const File& other) const noexcept -> bool {
        return (m_path == other.m_path)
                && (m_fileinfo == other.m_fileinfo)
                && (m_content == other.m_content);
    }

    [[nodiscard]] auto operator!=(const File& other) const noexcept -> bool {
        return !(*this == other);
    }

    [[nodiscard]] auto path() const noexcept -> const std::string& {
        return m_path;
    }

    [[nodiscard]] auto path() noexcept -> std::string& {
        return m_path;
    }

    [[nodiscard]] auto fileinfo() const noexcept -> const FileInfo& {
        return m_fileinfo;
    }

    [[nodiscard]] auto fileinfo() noexcept -> FileInfo& {
        return m_fileinfo;
    }

    [[nodiscard]] auto content() const noexcept -> const Blob& {
        return m_content;
    }

    [[nodiscard]] auto content() noexcept -> Blob& {
        return m_content;
    }

private:
    std::string m_path {};
    FileInfo m_fileinfo {};
    Blob m_content {};
};

} /* namespace census */
