// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include "util.hpp"

#include <map>
#include <string>
#include <vector>

namespace census {

class Diff {
public:
    [[nodiscard]] Diff(
            std::string field,
            std::string expected,
            std::string actual,
            std::string context = "") :
        m_field { std::move(field) },
        m_expected { std::move(expected) },
        m_actual { std::move(actual) },
        m_context { std::move(context) } {
    }

    [[nodiscard]] auto field() const noexcept -> const std::string& {
        return m_field;
    }

    [[nodiscard]] auto field() noexcept -> std::string& {
        return m_field;
    }

    [[nodiscard]] auto expected() const noexcept -> const std::string& {
        return m_expected;
    }

    [[nodiscard]] auto expected() noexcept -> std::string& {
        return m_expected;
    }

    [[nodiscard]] auto actual() const noexcept -> const std::string& {
        return m_actual;
    }

    [[nodiscard]] auto actual() noexcept -> std::string& {
        return m_actual;
    }

    [[nodiscard]] auto context() const noexcept -> const std::string& {
        return m_context;
    }

    [[nodiscard]] auto context() noexcept -> std::string& {
        return m_context;
    }

private:
    std::string m_field;
    std::string m_expected;
    std::string m_actual;
    std::string m_context;
};

using DiffList = std::vector<Diff>;
using DiffMap = std::map<std::string, DiffList>;

auto diff(
        DiffList& result,
        const FileInfo& expected,
        const FileInfo& actual,
        const std::string& context = "") -> void;

auto diff(
        DiffList& result,
        const Blob& expected,
        const Blob& actual,
        const std::string& context = "") -> void;

auto diff(
        DiffList& result,
        const File& expected,
        const File& actual,
        const std::string& context = "") -> void;

template<typename T>
auto diff(
        const T& expected,
        const T& actual,
        const std::string& context = "") -> DiffList {
    auto result = DiffList {};
    diff(result, expected, actual, context);
    return result;
}

auto validate(
        DiffMap& result,
        const std::vector<File>& files,
        const std::string& context = "") -> void;

} /* namespace census */
