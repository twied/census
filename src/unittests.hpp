// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include "diff.hpp"
#include "util.hpp"

#include <gtest/gtest.h>

#include <fcntl.h>
#include <filesystem>
#include <fstream>

class Environment : public ::testing::Environment {
public:
    auto SetUp() -> void override;

    auto TearDown() -> void override;

private:
    std::filesystem::path m_pwd {};
};
