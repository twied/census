// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "util.hpp"

#include <array>
#include <cstring>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <system_error>
#include <unistd.h>

namespace census {

Fd::Fd(const std::string& path, int flags, int mode) :
    /* NOLINTNEXTLINE(*-vararg) */
    m_fd { ::open(path.c_str(), flags, mode) } {

    if (m_fd < 0) {
        throw std::system_error { errno, std::generic_category(), path };
    }
}

Fd::~Fd() {
    if (m_fd < 0) {
        return;
    }

    ::close(m_fd);
}

Blob::Blob(size_t size) :
    m_size { size } {

    if (size == 0) {
        return;
    }

    m_ptr = static_cast<std::byte*>(mmap(
            nullptr,
            size,
            PROT_READ | PROT_WRITE,
            MAP_PRIVATE | MAP_ANONYMOUS,
            -1,
            0));

    if (m_ptr == MAP_FAILED) {
        throw std::system_error(errno, std::generic_category(), "mmap");
    }
}

Blob::Blob(const void* ptr, size_t size) :
    Blob { size } {

    if (size == 0) {
        return;
    }

    if ((ptr == nullptr) || (ptr == MAP_FAILED)) {
        throw std::runtime_error("Blob::Blob(nullptr, size>0)");
    }
    std::memcpy(m_ptr, ptr, size);
}

Blob::Blob(std::string_view string) :
    Blob { string.data(), string.size() } {
}

Blob::Blob(const Fd& filedescriptor, size_t size) :
    m_ptr { static_cast<std::byte*>(::mmap(
            nullptr,
            size,
            PROT_READ,
            MAP_PRIVATE,
            filedescriptor.get(),
            0)) },
    m_size { size } {

    if (m_ptr == MAP_FAILED) {
        throw std::system_error { errno, std::generic_category(), "mmap" };
    }
}

Blob::~Blob() {
    if (!*this) {
        return;
    }

    ::munmap(m_ptr, m_size);
}

auto Blob::operator==(const Blob& other) const noexcept -> bool {
    if (m_size != other.m_size) {
        return false;
    }

    return std::memcmp(m_ptr, other.m_ptr, m_size) == 0;
}

File::File(std::string path) :
    m_path { std::move(path) } {

    using struct_statx = struct statx;

    auto statfd = Fd { m_path, O_RDONLY | O_PATH | O_NOFOLLOW };

    auto buffer = struct_statx {};
    auto ret = statx(
            statfd.get(),
            "",
            AT_EMPTY_PATH | AT_SYMLINK_NOFOLLOW,
            STATX_ALL,
            &buffer);
    if (ret < 0) {
        throw std::system_error { errno, std::generic_category(), "statx" };
    }

    if ((buffer.stx_mask & STATX_UID) != 0) {
        m_fileinfo.uid() = buffer.stx_uid;
    }

    if ((buffer.stx_mask & STATX_GID) != 0) {
        m_fileinfo.gid() = buffer.stx_gid;
    }

    if ((buffer.stx_mask & STATX_MODE) != 0) {
        m_fileinfo.mode() = buffer.stx_mode;
    }

    if ((buffer.stx_mask & STATX_SIZE) != 0) {
        m_fileinfo.size() = buffer.stx_size;
    }

    if (S_ISREG(m_fileinfo.mode())) {
        if (m_fileinfo.size() > 0) {
            auto readfd = Fd {
                "/proc/self/fd/" + std::to_string(statfd.get()),
                O_RDONLY
            };
            m_content = Blob { readfd, m_fileinfo.size() };
        }
    } else if (S_ISLNK(m_fileinfo.mode())) {
        auto data = std::array<char, BUFSIZ> {};
        auto ret = ::readlinkat(statfd.get(), "", data.data(), data.size());
        if ((ret < 0) || (static_cast<unsigned>(ret) == data.size())) {
            throw std::system_error {
                errno,
                std::generic_category(),
                "readlinkat"
            };
        }

        m_content = Blob { data.data(), static_cast<unsigned>(ret) };
    } else if (S_ISDIR(m_fileinfo.mode())) {
        m_fileinfo.size() = 0;
    }
}

} /* namespace census */
