// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include "diff.hpp"
#include "util.hpp"

#include <vector>

namespace census {

auto read_archive(const Blob& blob) -> std::vector<File>;

auto read_archive_deb(const Blob& blob) -> std::vector<File>;

auto validate_package(DiffMap& result, const File& package) -> void;

auto validate_package(
        DiffMap& result,
        const std::string& package) -> void;

} /* namespace census */
