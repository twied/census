// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "system.hpp"

#include "package.hpp"

#include <algorithm>
#include <apt-pkg/acquire-item.h>
#include <apt-pkg/acquire.h>
#include <apt-pkg/cachefile.h>
#include <apt-pkg/hashes.h>
#include <apt-pkg/indexfile.h>
#include <apt-pkg/init.h>
#include <apt-pkg/pkgrecords.h>
#include <apt-pkg/pkgsystem.h>
#include <apt-pkg/sourcelist.h>
#include <filesystem>
#include <stdexcept>

namespace census {

struct DebPkg {
    std::string name;
    std::string arch;
    std::string version;
    std::string hash;
    std::string url;
};

static auto initialize_libapt() -> void {
    static auto initialized = false;

    if (!initialized) {
        if (!pkgInitConfig(*_config)) {
            throw std::runtime_error { "pkgInitConfig failed" };
        }

        if (!pkgInitSystem(*_config, _system)) {
            throw std::runtime_error { "pkgInitSystem failed" };
        }

        initialized = true;
    }
}

static auto package_name(const DebPkg& package) -> std::string {
    auto filename = std::string {};
    filename += package.name;
    filename += '_';
    for (const auto& character : package.version) {
        if (character == ':') {
            filename += "%3a";
            continue;
        }

        filename += character;
    }
    filename += '_';
    filename += package.arch;
    filename += ".deb";

    return filename;
}

static auto list_libapt() -> std::vector<DebPkg> {
    initialize_libapt();

    auto cache_file = pkgCacheFile {};
    if (!cache_file.Open(nullptr, false)) {
        throw std::runtime_error { "pkgCacheFile::Open() failed" };
    }

    auto* cache = cache_file.GetPkgCache();
    if (cache == nullptr) {
        throw std::runtime_error { "pkgCacheFile::GetPkgCache() failed" };
    }

    auto records = pkgRecords { *cache };

    auto source_list = pkgSourceList {};
    if (!source_list.ReadMainList()) {
        throw std::runtime_error { "pkgSourceList::ReadMainList() failed" };
    }

    auto retval = std::vector<DebPkg> {};
    for (auto it = cache->PkgBegin(); it != cache->PkgEnd(); ++it) {
        auto current = it.CurrentVer();
        if (current == nullptr) {
            continue;
        }

        auto hash = std::string {};
        auto url = std::string {};
        for (auto i = current.FileList(); !i.end(); ++i) {
            auto& parser = records.Lookup(i);

            auto hashes = parser.Hashes();
            if (hashes.usable()) {
                const auto* hashstring = hashes.find("SHA256");
                if (hashstring != nullptr && hashstring->usable()) {
                    hash = hashstring->HashValue();
                }
            }

            auto* index = static_cast<pkgIndexFile*>(nullptr);
            source_list.FindIndex(i.File(), index);
            if (index != nullptr) {
                url = index->ArchiveURI(parser.FileName());
            }
        }

        retval.emplace_back(DebPkg {
                it.Name(),
                current.Arch(),
                current.VerStr(),
                hash,
                url });
    }

    return retval;
}

auto system_package_manager() -> std::string {
    return "aptpkg";
}

auto system_list() -> std::vector<std::string> {
    auto packages = list_libapt();

    auto retval = std::vector<std::string> {};
    std::transform(
            packages.begin(),
            packages.end(),
            std::back_inserter(retval),
            package_name);

    std::sort(retval.begin(), retval.end());
    return retval;
}

static auto check_hash(const File& file, const std::string& hash) -> bool {
    if (hash.empty()) {
        return true;
    }

    auto hashes = Hashes { Hashes::SHA256SUM };
    hashes.Add(
            /* NOLINTNEXTLINE(*-reinterpret-cast) */
            reinterpret_cast<const unsigned char*>(file.content().data()),
            file.content().size());

    return hash == hashes.GetHashString(Hashes::SHA256SUM).HashValue();
}

static auto user_cache_dir() -> std::string {
    static auto retval = std::string {};

    if (retval.empty()) {
        const auto* home = static_cast<const char*>(getenv("HOME"));
        if (home == nullptr) {
            home = "/var/cache/census";
        }

        retval = std::string { home } + "/.cache/census/";
        std::filesystem::create_directories(retval);
    }

    return retval;
}

static auto find_package_cache(File& file, const DebPkg& package) -> bool {
    try {
        file = File { user_cache_dir() + package_name(package) };
    } catch (const std::exception&) {
        return false;
    }

    return check_hash(file, package.hash);
}

static auto find_package_download(File& file, const DebPkg& package) -> bool {
    if (package.url.empty()) {
        return false;
    }

    auto cache_dir = user_cache_dir();

    auto acquire = pkgAcquire {};

    auto pkgacqfile = pkgAcqFile {
        &acquire,
        package.url,
        HashStringList {},
        0,
        "",
        "",
        "",
        cache_dir + package_name(package),
        false
    };

    std::clog << "Downloading " << package_name(package) << '\n';
    acquire.Run();

    return find_package_cache(file, package);
}

static auto find_package_system(File& file, const DebPkg& package) -> bool {
    static const auto system_cache = std::string { "/var/cache/apt/archives/" };

    try {
        file = File { system_cache + package_name(package) };
    } catch (const std::exception&) {
        return false;
    }

    return check_hash(file, package.hash);
}

static auto find_package(File& file, const DebPkg& package) -> bool {
    if (find_package_cache(file, package)) {
        return true;
    }

    if (find_package_system(file, package)) {
        return true;
    }

    if (find_package_download(file, package)) {
        return true;
    }

    return false;
}

auto system_validate() -> DiffMap {
    auto packages = list_libapt();

    auto retval = DiffMap {};
    for (const auto& package : packages) {
        auto file = File {};
        if (!find_package(file, package)) {
            retval[package_name(package)].emplace_back(
                    "package",
                    "ok",
                    "not found");
            continue;
        }

        validate_package(retval, file);
    }
    return retval;
}

} /* namespace census */
