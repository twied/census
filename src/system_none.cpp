// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "system.hpp"

#include <stdexcept>

namespace census {

auto system_package_manager() -> std::string {
    return "none";
}

auto system_list() -> std::vector<std::string> {
    throw std::runtime_error { "not supported" };
}

auto system_validate() -> DiffMap {
    throw std::runtime_error { "not supported" };
}

} /* namespace census */
