// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

struct global_options {
    bool verbose;
};

auto options() -> global_options&;
